def menu():
    print("1:Append", end="\t\t\t")
    print("2.Delete", end="\t\t\t")
    print("3:Update")
    print("4:Search stNo", end="\t\t")
    print("5:Search name", end="\t\t")
    print("6:Report all")
    print("7:Exit")
    choose = int(input(" " * 30 + " Select 1 to 7:"))
    return choose


def findStNo(stNo):
    myFile = open("Student_project_file.txt", '+a')
    myFile.seek(0)
    lines = myFile.readlines()
    recNo = 0
    for line in lines:
        curRecord = line.split(',')
        if stNo == curRecord[0]:
            return recNo
        recNo = recNo + 1
    myFile.close()
    return -1


def findName(fname, lname):
    myFile = open("Student_project_file.txt", '+a')
    myFile.seek(0)
    lines = myFile.readlines()
    recNo = 0
    for line in lines:
        curRecord = line.split(',')
        if fname == curRecord[1] and lname == curRecord[2]:
            return recNo
        recNo = recNo + 1
    myFile.close()
    return -1


def insert():
    stNo = input("Enter stNo:")
    recNo = findStNo(stNo)
    if recNo == -1:
        fname = input("Enter first name:")
        lname = input("Enter last name:")
        average = float(input("Enter average:"))
        line = stNo + ',' + fname + ',' + lname + ',' + str(average) + "\n"
        myFile = open("Student_project_file.txt", '+a')
        myFile.write(line)
        myFile.close()
    else:
        myFile = open("Student_project_file.txt")
        lines = myFile.readlines()
        curRecord = lines[recNo].split(',')
        printInfo(curRecord)
        myFile.close()
    return


def printInfo(curRecord):
    print("-" * 40)
    print("Student number : ", curRecord[0])
    print("First name : ", curRecord[1])
    print("Last name : ", curRecord[2])
    print("Average: ", curRecord[3])
    print("-" * 40)
    return


def delete():
    stNo = input("Enter stNo:")
    recNo = findStNo(stNo)
    if recNo != -1:
        myFile = open("Student_project_file.txt")
        lines = myFile.readlines()
        curRecord = lines[recNo].split(',')
        printInfo(curRecord)
        ans = input("Are you sure that is Delete[y/n]?")
        if ans == 'y' or ans == 'Y':
            del lines[recNo]
        myFile.close()
        myFile = open("Student_project_file.txt", 'w')
        myFile.writelines(lines)
        myFile.close()
    else:
        print(" " * 12, "student number ", stNo, " not found in file")


def update():
    stNo = input("Enter stNo:")
    recNo = findStNo(stNo)
    if recNo != -1:
        myFile = open("Student_project_file.txt")
        lines = myFile.readlines()
        curRecord = lines[recNo].split(',')
        printInfo(curRecord)
        fname = input("Enter first name:")
        lname = input("Enter last name:")
        average = float(input("Enter average:"))
        line = stNo + ',' + fname + ',' + lname + ',' + str(average) + "\n"
        ans = input("Are you sure that is update[y/n]?")
        if ans == "y" or ans == "Y":
            lines[recNo] = line
            myFile.close()
            myFile = open("Student_project_file.txt", 'w')
            myFile.writelines(lines)
            myFile.close()
        else:
            print(" " * 12, "student number", stNo, " not found in file")


def searchName():
    fname = input("Enter first name:")
    lname = input("Enter last name:")
    recNo = findName(fname, lname)
    if recNo != -1:
        myFile = open("Student_project_file.txt")
        lines = myFile.readlines()
        curRecord = lines[recNo].split(',')
        printInfo(curRecord)
        myFile.close()
    else:
        print(" " * 10, "first name", fname, "last name", lname, "not found in file")


def searchStNoo():
    stNo = input("Enter stNo:")
    recNo = findStNo(stNo)
    if recNo != -1:
        myFile = open("Student_project_file.txt")
        lines = myFile.readlines()
        curRecord = lines[recNo].split(',')
        printInfo(curRecord)
        myFile.close()
    else:
        print(" " * 12, "student number", stNo, "not found in file")


def reportAllo():
    myFile = open("Student_project_file.txt", 'r')
    lines = myFile.readlines()
    print("stNo", end="\t\t")
    print("f_name", end="\t\t")
    print("l_name", end="\t\t")
    print("Average ")
    print("=" * 70)
    sum = 0
    for line in lines:
        curRecord = line.split(',')
        print(curRecord[0], end="\t\t")
        print(curRecord[1], end="t\t")
        print(curRecord[2], end="t\t")
        print(curRecord[3])
        sum = sum + float(curRecord[3])
        if len(lines) > 0:
            print("" * 30, "Total Average = ", sum / len(lines))
        myFile.close()
    return


while True:
    choose = menu()
    if choose == 7:
        break
    elif choose == 1:
        insert()
    elif choose == 2:
        delete()
    elif choose == 3:
        update()
    elif choose == 4:
        searchStNoo()
    elif choose == 5:
        searchName()
    elif choose == 6:
        reportAllo()
    else:
        menu()
    print("*" * 75)
